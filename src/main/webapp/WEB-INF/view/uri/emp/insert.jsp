<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<c:if test="${rMap!=null}">
	<c:if test="${rMap.result=='true'}">
	<script>
		location.href="/uri/emp/login";
	</script>
	</c:if>
</c:if>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>로그인 화면</title>
    
    <!-- Bootstrap core CSS -->
    <link href="/resources/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/resources/signin.css" rel="stylesheet">
  </head>
	<script>
		window.addEventListener('load', function(){
			document.querySelector('#signinBtn').onclick = function(){
				var enameObj = document.querySelector('#ename');
				var mgrObj = document.querySelector('#mgr');
				var commObj = document.querySelector('#comm');
				var salObj = document.querySelector('#sal');
				var hiredateObj = document.querySelector('#hiredate');
				var JobObj = document.querySelector('#job').value;
				var deptnoObj = document.querySelector('#deptno').value;

			var xhr = new XMLHttpRequest();
			xhr.open('POST','/emp/insert');
			xhr.setRequestHeader('content-type','application/x-www-form-urlencoded');
			xhr.onreadystatechange = function(){
				if(xhr.readyState==4){
					var obj = JSON.parse(xhr.response);
					if(obj.result==='false'){
						alert('회원가입 실패');
						location.href = "/emp/insert"
					}else{
						alert("성공");
						location.href="/uri/emp/login";
					}
				console.log(xhr.response);
			}
		}
			var param='ename=' + enameObj.value + '&mgr='+ mgrObj.value +'&comm=' + commObj.value + '&sal=' + salObj.value + '&hiredate=' + hiredateObj.value + '&Job='+ document.getElementById("job").options[document.getElementById("job").selectedIndex].value + '&deptno=' + document.getElementById("deptno").options[document.getElementById("deptno").selectedIndex].value;
			xhr.send(param);
		}
	});
		
	</script>
  <body class="text-center">
    <form class="form-signin" action="/emp/insert" method="POST">
      <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      
      
      <label for="inputName" class="sr-only">Name</label>
      <input type="text" id="ename" class="form-control" placeholder="이름" required
      name="ename">
      
      <label for="inputMgr" class="sr-only">MGR</label>
      <input type="number" id="mgr" class="form-control" placeholder="매니저" required
      name="mgr">
      
      <br>
      
      <label for="inputComm" class="sr-only">Comm</label>
      <input type="text" id="comm" class="form-control" placeholder="커미션" required
      name="comm">
      
      <label for="inputSal" class="sr-only">Sal</label>
      <input type="text" id="sal" class="form-control" placeholder="월급                                 단위 : 만원" required
      name="sal">
      
      <br>
      
        
    <br>
     <label for="inputHireDate" class="sr-only">HireDate</label>
     <input type="date" id="hiredate" class="form-control" placeholder="입사년월" required
      name="hiredate">
  
    <select name = "Job" id="job" class="form-control" required
      name="Job">  
    	<option value="사원">사원</option>
    	<option value="대리">대리</option>
    	<option value="과장">과장</option>
    	<option value="부장">부장</option>
    	<option value="차장">차장</option>    	
    </select>
    <select name = "deptno" id="deptno" class="form-control" >
    	<c:forEach items="${deptList}" var="dept">
    		<option value="${dept.DEPTNO}"> ${dept.DNAME} </option>
    	</c:forEach>
    	
    </select>
   
 
    
       
      
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" id="signinBtn" type="button">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form>
  </body>
</html>
